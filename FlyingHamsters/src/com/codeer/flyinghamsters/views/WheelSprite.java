package com.codeer.flyinghamsters.views;

import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.math.MathUtils;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

public class WheelSprite extends Sprite {
	private Vector2 a;
	private Vector2 b;
	private Hamster h;
	public WheelSprite(float pX, float pY, ITextureRegion itr, VertexBufferObjectManager vbom) {
		super(pX, pY, itr, vbom);
		this.a = new Vector2();
		this.b = new Vector2(this.getX(), this.getY());
	}
	public void setHamster(Hamster ham) {
		this.h = ham;
	}
	@Override
	public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAtreaLocalX, 
			final float pTouchAreaLocalY){
		Body myBody = (Body) getUserData();
		switch(pSceneTouchEvent.getAction()) {
			case TouchEvent.ACTION_DOWN:
				a.x = pSceneTouchEvent.getX();
				a.y = pSceneTouchEvent.getY();
				break;
			case TouchEvent.ACTION_MOVE:
				Vector2 ab = new Vector2(b.x - a.x, b.x - a.x);
				Vector2 cb = new Vector2(b.x - pSceneTouchEvent.getX(), b.y - pSceneTouchEvent.getY());
				float dot = (ab.x*cb.x + ab.y*cb.y);
				float cross = (ab.x*cb.y - ab.y*cb.x);
				float alpha = (float) Math.atan2(cross, dot);
				float degrees = MathUtils.radToDeg(alpha);
				myBody.applyAngularImpulse(myBody.getMass()*degrees);
				a.x = pSceneTouchEvent.getX();
				a.y = pSceneTouchEvent.getY();
				break;
			case TouchEvent.ACTION_UP:
				this.h.throwHamster(myBody.getAngularVelocity());
				myBody.setAngularVelocity(0);
				break;
		}
		return true;
	}
}
