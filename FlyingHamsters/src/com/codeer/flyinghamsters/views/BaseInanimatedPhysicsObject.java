package com.codeer.flyinghamsters.views;

import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;

public class BaseInanimatedPhysicsObject {
	private Sprite s;
	private Body b;
	public BaseInanimatedPhysicsObject(Scene sc, float pX, float pY, ITextureRegion itr, VertexBufferObjectManager vbom,
			ShapeType sht, PhysicsWorld phys, BodyType bodyType, FixtureDef fxdef) {
		this.s = new Sprite(pX, pY, itr, vbom);
		sc.attachChild(s);
		this.b = this.createBody(sht, phys, bodyType, fxdef);
	}
	private Body createBody(ShapeType sht, PhysicsWorld phys, BodyType bodyType, FixtureDef fxdef) {
		if (sht == ShapeType.Box) {
			return PhysicsFactory.createBoxBody(phys, this.s, bodyType, fxdef);
		} else if (sht == ShapeType.Circle) {
			return PhysicsFactory.createCircleBody(phys, this.s, bodyType, fxdef);
		} else {
			return null;
		}
	}
	protected void setRotationCenter(float pX, float pY) {
		s.setRotationCenter(pX, pY);
	}
	protected void connectBodies(RevoluteJointDef rjdef, Body first, Vector2 where) {
		rjdef.initialize(first, this.b, where);
	}
}
